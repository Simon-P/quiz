
<form method="post" action="Result.php">
    <label>Kolik je 158*4?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot1" id="inLineRadio1" value="632">
        <label class ="form-check form-check-label" for="inLineRadio1">632</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot1" id="inLineRadio2" value="582">
        <label class ="form-check form-check-label" for="inLineRadio2">582</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot1" id="inLineRadio3" value="732">
        <label class ="form-check form-check-label" for="inLineRadio3">732</label>
    </div>
    <label>Kolik je 7*7*7*7*8?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot2" id="inLineRadio1" value="19108">
        <label class ="form-check form-check-label" for="inLineRadio1">19 108</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot2" id="inLineRadio2" value="19348">
        <label class ="form-check form-check-label" for="inLineRadio2">19 348</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot2" id="inLineRadio3" value="19208">
        <label class ="form-check form-check-label" for="inLineRadio3">19 208</label>
    </div>

    <label>Kolik je 64-68*9?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot3" id="inLineRadio1" value="-36">
        <label class ="form-check form-check-label" for="inLineRadio1">-36</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot3" id="inLineRadio2" value="-696">
        <label class ="form-check form-check-label" for="inLineRadio2">-696</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot3" id="inLineRadio3" value="-548">
        <label class ="form-check form-check-label" for="inLineRadio3">-548</label>
    </div>

    <label>Kolik je 458:0?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot4" id="inLineRadio1" value="1">
        <label class ="form-check form-check-label" for="inLineRadio1">1</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot4" id="inLineRadio2" value="correct">
        <label class ="form-check form-check-label" for="inLineRadio2">Nelze vypočítat</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot4" id="inLineRadio3" value="0">
        <label class ="form-check form-check-label" for="inLineRadio3">0</label>
    </div>

    <label>Kolik je 483:0,01?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot5" id="inLineRadio1" value="4830">
        <label class ="form-check form-check-label" for="inLineRadio1">4 830</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot5" id="inLineRadio2" value="483000">
        <label class ="form-check form-check-label" for="inLineRadio2">483 000</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot5" id="inLineRadio3" value="48300">
        <label class ="form-check form-check-label" for="inLineRadio3">48 300</label>
    </div>
    <label>Kolik je 1:100?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot6" id="inLineRadio1" value="0,01">
        <label class ="form-check form-check-label" for="inLineRadio1">0,01</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot6" id="inLineRadio2" value="0,1">
        <label class ="form-check form-check-label" for="inLineRadio2">0,1</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot6" id="inLineRadio3" value="0,00001">
        <label class ="form-check form-check-label" for="inLineRadio3">0,00001</label>
    </div>
    <label>Kolik je 420-58:2?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot7" id="inLineRadio1" value="491">
        <label class ="form-check form-check-label" for="inLineRadio1">491</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot7" id="inLineRadio2" value="391">
        <label class ="form-check form-check-label" for="inLineRadio2">391</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot7" id="inLineRadio3" value="181">
        <label class ="form-check form-check-label" for="inLineRadio3">181</label>
    </div>
    <label>Kolik je (85*2)-67?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot8" id="inLineRadio1" value="103">
        <label class ="form-check form-check-label" for="inLineRadio1">103</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot8" id="inLineRadio2" value="153">
        <label class ="form-check form-check-label" for="inLineRadio2">153</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot8" id="inLineRadio3" value="102">
        <label class ="form-check form-check-label" for="inLineRadio3">102</label>
    </div>

    <label>Kolik je (92:4)+54*35?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot9" id="inLineRadio1" value="2018">
        <label class ="form-check form-check-label" for="inLineRadio1">2 018</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot9" id="inLineRadio2" value="1815">
        <label class ="form-check form-check-label" for="inLineRadio2">1 815</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot9" id="inLineRadio3" value="1913">
        <label class ="form-check form-check-label" for="inLineRadio3">1 913</label>
    </div>

    <label>Kolik je 0:0?</label>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot10" id="inLineRadio2" value="1">
        <label class ="form-check form-check-label" for="inLineRadio2">1</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot10" id="inLineRadio3" value="nope">
        <label class ="form-check form-check-label" for="inLineRadio3">Nelze vypočítat</label>
    </div>
    <div class="form-check form-check-inline">
        <input class ="form-check form-check-inline" type="radio" name="ot10" id="inLineRadio4" value="0">
        <label class ="form-check form-check-label" for="inLineRadio4">0</label>
    </div>
    <input type="submit">
</form>

